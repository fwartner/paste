paste
=====

A Pastebin clone. Powered by CodeIgniter &amp; Bootstrap.. Oh! And Ace.

![Screenshot](https://raw.githubusercontent.com/fwndev/paste/master/screenshot.png)

### Setup:
You may have to cleanup the MySQL Dump-File. Simply upload all the files to your server and rename the "txt.htaccess" to ".htaccess".  
  
Edit the database settings in `application/config/database.php` matching to your credentials.

That´s all.

### Powered by:
[CodeIgniter](https://github.com/EllisLab/CodeIgniter/)  
[Twitter Bootstrap](https://github.com/twbs/bootstrap)  
[Ajax.org´s Ace](https://github.com/ajaxorg/ace)