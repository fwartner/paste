<?php defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'main';
$route['(:any)'] = 'main/$1';
$route['view/(:any)'] = 'main/view/$1';
$route['pastie/(:any)'] = 'pastie/view/$1';
$route['auth/(:any)'] = 'auth/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
