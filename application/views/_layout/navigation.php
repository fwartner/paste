
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url() ?>">paste</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo base_url() ?>">Home</a></li>
                <li><a href="<?php echo base_url() ?>">New Paste</a></li>
            </ul>
            <?php if (!$this->ion_auth->logged_in()) { ?>
                <form class="navbar-form navbar-right" role="form" action="<?php echo base_url('auth/login') ?>" method="post">
                    <div class="form-group">
                        <input type="text" placeholder="Username" name="identity" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" name="password" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-success">Sign in</button>
                    <a type="submit" class="btn btn-success" href="<?php echo base_url('auth/register') ?>">Register</a>
                </form>
            <?php } else { ?>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $user->username ?> <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">My Pasties</a></li>
                            <li><a href="#">Edit Account</a></li>
                            <li><a href="#">View Account</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url('auth/logout') ?>">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            <?php } ?>
        </div>
    </div>
</div>