
<div class="container">
    <div class="row">

        <form action="<?php echo base_url('main/create') ?>" id="edit" name="edit" method="post">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Drop your code!</h3>
                </div>
                <div class="panel-body">
                    <script type='text/javascript'>//<![CDATA[
                        $(function(){
                            var textarea = $('#content');

                            var editor = ace.edit("editor");
                            editor.setTheme("ace/theme/github");
                            editor.getSession().setMode("ace/mode/php");

                            editor.getSession().on('change', function () {
                                textarea.val(editor.getSession().getValue());
                            });

                            textarea.val(editor.getSession().getValue());
                        });//]]>

                    </script>
                    <div id="editor"></div>
                    <textarea name="content" id="content" style="visibility: hidden;"></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Options</h3>
                </div>
                <div class="panel-body">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter title"> <br>
                    <button type="submit" id="submit" class="btn btn-sm btn-primary"">Submit</button>
                    <a type="button" class="btn btn-sm btn-default" href="<?php echo base_url('raw/') ?>">Show Raw</a>
                    <a type="button" class="btn btn-sm btn-info" href="<?php echo base_url('edit/') ?>">Edit</a>
                </div>
            </div>
        </form>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Recent Pasties</h3>
                </div>
                <div class="panel-body">
                    <?php $i = 0; foreach ($pasties as $pastie_item): if ($i == 6) { break; } ?>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="<?php echo base_url('pastie/' . $pastie_item['slug']) ?>"><?php echo $pastie_item['title'] ?></a></li>
                        </ul>
                    <?php $i++; endforeach ?>
                </div>
            </div>
        </div>

    </div>

