<?php

class Paste_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_pasties($slug = FALSE) {
        if ($slug === FALSE) {
            $query = $this->db->get('pasties');
            return $query->result_array();
        }

        $query = $this->db->get_where('pasties', array('slug' => $slug));
        return $query->row_array();
    }

    public function set_pastie() {
        $this->load->helper('url');
        $this->load->helper('string');

        $slug = md5('pastie' . random_string('alnum', 16));

        $data = array(
            'title' => $this->input->post('title'),
            'slug' => $slug,
            'text' => $this->input->post('content')
        );

        return $this->db->insert('pasties', $data);
    }
}