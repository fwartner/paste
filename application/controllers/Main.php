<?php

class Main extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('paste_model');
    }

    public function index() {
        $this->data['title'] = 'Home';
        $this->data['user'] = $this->ion_auth->user()->row();
        $this->data['pasties'] = $this->paste_model->get_pasties();

        $this->load->view('_layout/header', $this->data);
        $this->load->view('_layout/navigation', $this->data);
        $this->load->view('home', $this->data);
        $this->load->view('_layout/footer', $this->data);
    }

    public function view($slug) {
        $this->data['user'] = $this->ion_auth->user()->row();
        $this->data['pastie_item'] = $this->paste_model->get_pasties($slug);
        $this->data['pasties'] = $this->paste_model->get_pasties();

        if (empty($this->data['pastie_item'])) {
            show_404();
        }

        $this->data['title'] = $this->data['pastie_item']['title'];

        $this->load->view('_layout/header', $this->data);
        $this->load->view('_layout/navigation', $this->data);
        $this->load->view('view', $this->data);
        $this->load->view('_layout/footer', $this->data);
    }

    public function create() {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->data['user'] = $this->ion_auth->user()->row();
        $this->data['title'] = 'Create new pastie';

        $this->load->view('_layout/header', $this->data);
        $this->load->view('_layout/navigation', $this->data);
        $this->load->view('home', $this->data);
        $this->load->view('_layout/footer', $this->data);

        $this->paste_model->set_pastie();
        redirect('/');
    }

    public function show_raw($slug) {
        $this->data['user'] = $this->ion_auth->user()->row();
        $this->data['pastie_item'] = $this->paste_model->get_pasties($slug);
        $this->data['pasties'] = $this->paste_model->get_pasties();

        if (empty($this->data['pastie_item'])) {
            show_404();
        }

        $this->data['title'] = $this->data['pastie_item']['title'];
        $this->load->view('raw', $this->data);
    }
}